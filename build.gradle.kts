import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

//buildscript {
//	repositories {
//		maven {
//			url = uri("https://plugins.gradle.org/m2/")
//		}
//	}
//	dependencies {
//		classpath("net.corda.plugins:cordapp:4.0.44")
//		classpath("net.corda.plugins:cordformation:4.0.44")
//		classpath("net.corda.plugins:publish-utils:4.0.44")
//	}
//}

plugins {
	kotlin("plugin.jpa") version "1.2.71"
	id("org.springframework.boot") version "2.1.6.RELEASE"
	id("io.spring.dependency-management") version "1.0.7.RELEASE"
	kotlin("jvm") version "1.2.71"
	kotlin("plugin.spring") version "1.2.71"
//	id("net.corda.plugins.cordapp") version "4.0.44"
//	id("net.corda.plugins.cordformation") version "4.0.44"
//	id("net.corda.plugins.publish-utils") version "4.0.44"

}

group = "com.example"
version = "0.0.1-SNAPSHOT"
java.sourceCompatibility = JavaVersion.VERSION_1_8

val developmentOnly by configurations.creating
configurations {
	runtimeClasspath {
		extendsFrom(developmentOnly)
	}
}

repositories {
	mavenCentral()
}

extra["springCloudVersion"] = "Greenwich.SR1"

//apply(plugin = "net.corda.plugins.cordapp")
//apply(plugin = "net.corda.plugins.cordformation")
//apply(plugin = "net.corda.plugins.publish-utils")

dependencies {
	implementation("org.springframework.boot:spring-boot-starter-actuator")
	implementation("org.springframework.boot:spring-boot-starter-data-jpa")
	implementation("org.springframework.boot:spring-boot-starter-web")
	implementation("com.fasterxml.jackson.module:jackson-module-kotlin")
	implementation("org.jetbrains.kotlin:kotlin-reflect")
	implementation("org.jetbrains.kotlin:kotlin-stdlib-jdk8")
	implementation("org.springframework.boot:spring-boot-starter-amqp")
//	implementation("net.logstash.logback:logstash-logback-encoder:5.3")
//	implementation("ch.qos.logback:logback-core:1.2.3")

	/**
	 * https://cloud.spring.io/spring-cloud-sleuth/2.0.x/multi/multi__current_span.html
	 * https://cloud.spring.io/spring-cloud-sleuth/reference/html/
	 * https://medium.com/linedevth/%E0%B8%AA%E0%B8%A3%E0%B9%89%E0%B8%B2%E0%B8%87%E0%B8%A3%E0%B8%B0%E0%B8%9A%E0%B8%9A-monitoring-microservices-%E0%B8%94%E0%B9%89%E0%B8%A7%E0%B8%A2-spring-cloud-sleuth-elk-%E0%B9%81%E0%B8%A5%E0%B8%B0-zipkin-284d1aca16b4
	 * https://dzone.com/articles/distributed-tracing-with-zipkin-and-elk
	 * https://dzone.com/articles/monitoring-microservices-with-spring-cloud-sleuth
	 */
	implementation("org.springframework.cloud:spring-cloud-starter-sleuth")
	implementation("org.springframework.cloud:spring-cloud-starter-zipkin")
	developmentOnly("org.springframework.boot:spring-boot-devtools")
	runtimeOnly("com.h2database:h2")
	testImplementation("org.springframework.boot:spring-boot-starter-test")
}

dependencyManagement {
	imports {
		mavenBom("org.springframework.cloud:spring-cloud-dependencies:${property("springCloudVersion")}")
	}
}

tasks.withType<KotlinCompile> {
	kotlinOptions {
		freeCompilerArgs = listOf("-Xjsr305=strict")
		jvmTarget = "1.8"
	}
}
