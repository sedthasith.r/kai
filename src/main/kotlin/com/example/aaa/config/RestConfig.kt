package com.example.aaa.config

import org.springframework.boot.web.client.RestTemplateBuilder
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.web.client.RestTemplate

@Configuration
class RestConfig {

    @Bean
    fun restTemplate(restTemplateBuilder: RestTemplateBuilder): RestTemplate {
        return restTemplateBuilder.build()
//        return restTemplateBuilder
////                .requestFactory(BufferingClientHttpRequestFactory(SimpleClientHttpRequestFactory()))
//                .interceptors(CustomInterceptor())
//                //				.messageConverters(setMappingJackson2HttpMessageConverter())
//                .setConnectTimeout(10000)
//                .setReadTimeout(10000)
//                .build()
    }
}