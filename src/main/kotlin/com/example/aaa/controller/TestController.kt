package com.example.aaa.controller

import brave.Tracer
import com.example.aaa.service.Sender
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.*
import org.springframework.web.client.RestTemplate

@RestController
@RequestMapping("aaa")
class TestController {

    companion object {
        private val logger: Logger = LoggerFactory.getLogger(this::class.java)
    }

    @Autowired
    lateinit var rest: RestTemplate

    @Autowired
    lateinit var tracer: Tracer

    @Autowired
    lateinit var sender: Sender

    @GetMapping("")
    fun test(@RequestParam(defaultValue = "http://localhost:8081/bbb", required = false) url: String ): String {
        val rs = rest.getForObject(url, String::class.java)
        logger.info("rs = $rs")
        logger.info("tracer = ${tracer.currentSpan().context()}")
        logger.info("tracer parentIdString = ${tracer.currentSpan().context().parentIdString()}")
        logger.info("tracer traceIdString = ${tracer.currentSpan().context().traceIdString()}")
        logger.info("tracer span = ${tracer.currentSpan().context().spanIdString()}")
        logger.info("tracer = ${tracer.nextSpan().context().traceIdString()}")

        return rs!!
    }

    @GetMapping("/apatha/{path}")
    fun test1(@PathVariable("path") path: String): String {
        logger.info("apatha $path")
        logger.info("tracer = ${tracer.currentSpan().context()}")
        logger.info("tracer parentIdString = ${tracer.currentSpan().context().parentIdString()}")
        logger.info("tracer traceIdString = ${tracer.currentSpan().context().traceIdString()}")
        logger.info("tracer span = ${tracer.currentSpan().context().spanIdString()}")
        logger.info("tracer = ${tracer.nextSpan().context().traceIdString()}")
        return path
    }

    @GetMapping("/bbb")
    fun getFromB(@RequestParam(defaultValue = "http://localhost:8081/bbb", required = false) url: String ): String {
        val rs = rest.getForObject(url, String::class.java)
        logger.info("getFromB $url")
        return url
    }

    @GetMapping("/ccc")
    fun getFromC(@RequestParam(defaultValue = "http://localhost:8082/ccc", required = false) url: String ): String {
        val rs = rest.getForObject(url, String::class.java)
        logger.info("getFromC $url")
        return url
    }

    @PostMapping("/body")
    fun testPost(@RequestBody map: Map<String, Any>): Map<String, Any> {

        logger.info("testPost $map")
        return map
    }

    @GetMapping("/test")
    fun test(@RequestParam(value="msg", defaultValue = "deeja") msg: String,
             @RequestParam(value="priority", defaultValue = "0") priority: Int,
             @RequestParam(value="routingKey", defaultValue = "") routingKey: String) {
        sender.send(msg, priority, routingKey)
    }

}