package com.example.aaa.service

import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.amqp.core.MessageProperties
import org.springframework.amqp.rabbit.core.RabbitTemplate
import org.springframework.amqp.support.converter.SimpleMessageConverter
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service


@Service
class Sender {

    companion object {
        private val logger: Logger = LoggerFactory.getLogger(this::class.java)
    }

    @Autowired
    lateinit var rabbitTemplate: RabbitTemplate

    fun send(msg: String, priority: Int, routingKey: String) {
//        val messagePostProcessor = MessagePostProcessor().postProcessMessage("")
//        val messagePostProcessor = MyMessagePostProcessor()

//        val converter = SimpleMessageConverter()
//
//
//        val props = MessageProperties()
//        props.priority = 10
//        val toSend = Message(msg.toByteArray(), props)
//
////        var messageObject: Any? = null
////        messageObject = msg
//
//        rabbitTemplate.convertAndSend("exchange", "", "$msg")
//        println("b4")
//        rabbitTemplate.send("exchange", "", toSend)
//        println("after")


        val messageConverter = SimpleMessageConverter()
        val messageProperties = MessageProperties()
        messageProperties.priority = priority
        val message = messageConverter.toMessage(msg, messageProperties)

        logger.info("b4 ${message}")
//        rabbitTemplate.convertAndSend("topic.exchange", routingKey, message)
        rabbitTemplate.convertAndSend("direct.exchange", routingKey, message)
        logger.info("after")
    }
}